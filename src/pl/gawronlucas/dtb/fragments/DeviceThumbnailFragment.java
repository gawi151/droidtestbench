package pl.gawronlucas.dtb.fragments;

import pl.gawronlucas.dtb.R;
import pl.gawronlucas.dtb.data.Device;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link DeviceThumbnailFragment.OnFragmentInteractionListener} interface to
 * handle interaction events.
 * 
 */
public class DeviceThumbnailFragment extends Fragment {

	private OnFragmentInteractionListener mListener;

	public DeviceThumbnailFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_device_thumbnail, container,
				false);
		
		TextView deviceName = (TextView) v.findViewById(R.id.textView_deviceName);
		deviceName.setText(Device.DEVICE);
		TextView deviceCpu = (TextView) v.findViewById(R.id.textView_deviceCpu);
		deviceCpu.setText(Device.CPU_ABI);
		
		return v;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		public void onFragmentInteraction(Uri uri);
	}

}
