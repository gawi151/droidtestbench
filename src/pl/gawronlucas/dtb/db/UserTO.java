package pl.gawronlucas.dtb.db;

/**
 * Klasa odzierwciedlająca tabelę User w bazie danych.
 * @author Łukasz Gawron
 * 
 */

public class UserTO {
	/**
	 * Prywatne pola odzwierciedlające kolumny w bazie danych.
	 */
	private String userName,
					password,
					firstName,
					lastName,
					country,
					city;
	private int id;
	
	
	
	public UserTO(){
	}
	/**
	 * Constructor of User class
	 * @param id the id to set
	 * @param username the user name to set
	 * @param password the password to set
	 */
	public UserTO(int id, String username, String password){
		setId(id);
		setUserName(username);
		setPassword(password);
	}
	/**
	 * Constructor of User class
	 * @param id
	 * @param username
	 * @param password
	 * @param firstname
	 */
	public UserTO(int id, String username, String password, String firstname){
		setId(id);
		setUserName(username);
		setPassword(password);
		setFirstName(firstname);
	}
	/**
	 * Constructor of User class
	 * @param id
	 * @param username
	 * @param password
	 * @param firstname
	 * @param lastname
	 */
	public UserTO(int id, String username, String password, String firstname, String lastname){
		setId(id);
		setUserName(username);
		setPassword(password);
		setFirstName(firstname);
		setLastName(lastname);
	}
	/**
	 * Constructor of User class
	 * @param id
	 * @param username
	 * @param password
	 * @param firstname
	 * @param lastname
	 * @param country
	 */
	public UserTO(int id, String username, String password, String firstname, String lastname, String country){
		setId(id);
		setUserName(username);
		setPassword(password);
		setFirstName(firstname);
		setLastName(lastname);
		setCountry(country);
	}
	/**
	 * Constructor of User class
	 * @param id
	 * @param username
	 * @param password
	 * @param firstname
	 * @param lastname
	 * @param country
	 * @param city
	 */
	public UserTO(int id, String username, String password, String firstname, String lastname, String country, String city){
		setId(id);
		setUserName(username);
		setPassword(password);
		setFirstName(firstname);
		setLastName(lastname);
		setCountry(country);
		setCity(city);
	}	
	
	/**
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * @param username the user name to set
	 */
	public void setUserName(String username) {
		this.userName = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	
	

}
