package pl.gawronlucas.dtb.db;

/**
 * 
 * @author �ukasz Gawron
 *
 */

public class CategoryTO {
	
	private int id;
	private String categoryName;
	
	
	public CategoryTO(){
	}
	
	public CategoryTO(int id){
		setId(id);
	}

	public CategoryTO(int id, String categoryName){
		setId(id);
		setCategoryName(categoryName);
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
