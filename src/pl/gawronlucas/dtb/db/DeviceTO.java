package pl.gawronlucas.dtb.db;

/**
 * 
 * @author �ukasz Gawron
 *
 */

public class DeviceTO {

	private String board;
	private String brand;
	private String cpu_abi;
	private String cpu_abi2;
	private String device;
	private String manufacturer;
	private String model;
	private String product;
	private String hardware;
	private String bootloader;
	private int sdk;
	private int id;
	
	/**
	 * Default constructor
	 */
	public DeviceTO(){	
	}
	
	/**
	 * Constructor
	 * @param id the id to set
	 */
	public DeviceTO(int id){
		setId(id);
	}
	
	/**
	 * @return the board
	 */
	public String getBoard() {
		return board;
	}
	/**
	 * @param board the board to set
	 */
	public void setBoard(String board) {
		this.board = board;
	}
	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}
	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}
	/**
	 * @return the cpu_abi
	 */
	public String getCpu_abi() {
		return cpu_abi;
	}
	/**
	 * @param cpu_abi the cpu_abi to set
	 */
	public void setCpu_abi(String cpu_abi) {
		this.cpu_abi = cpu_abi;
	}
	/**
	 * @return the cpu_abi2
	 */
	public String getCpu_abi2() {
		return cpu_abi2;
	}
	/**
	 * @param cpu_abi2 the cpu_abi2 to set
	 */
	public void setCpu_abi2(String cpu_abi2) {
		this.cpu_abi2 = cpu_abi2;
	}
	/**
	 * @return the device
	 */
	public String getDevice() {
		return device;
	}
	/**
	 * @param device the device to set
	 */
	public void setDevice(String device) {
		this.device = device;
	}
	/**
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return manufacturer;
	}
	/**
	 * @param manufacturer the manufacturer to set
	 */
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}
	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}
	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(String product) {
		this.product = product;
	}
	/**
	 * @return the hardware
	 */
	public String getHardware() {
		return hardware;
	}
	/**
	 * @param hardware the hardware to set
	 */
	public void setHardware(String hardware) {
		this.hardware = hardware;
	}
	/**
	 * @return the bootloader
	 */
	public String getBootloader() {
		return bootloader;
	}
	/**
	 * @param bootloader the bootloader to set
	 */
	public void setBootloader(String bootloader) {
		this.bootloader = bootloader;
	}
	/**
	 * @return the sdk
	 */
	public int getSdk() {
		return sdk;
	}
	/**
	 * @param sdk the sdk to set
	 */
	public void setSdk(int sdk) {
		this.sdk = sdk;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
}
