package pl.gawronlucas.dtb.db;

/**
 * 
 * @author �ukasz Gawron
 *
 */

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DtbOpenHelper extends SQLiteOpenHelper {
	
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "DTB.db";
	
    public static final String USER_TABLE_NAME = "user";
    public static final String USER_ID = "id";
    public static final String USER_USERNAME = "username";
    public static final String USER_PASSWORD = "password";
    public static final String USER_FIRSTNAME = "first_name";
    public static final String USER_LASTNAME = "last_name";
    public static final String USER_COUNTRY = "country";
    public static final String USER_CITY = "city";
    private static final String USER_TABLE_CREATE =
                "CREATE TABLE " + USER_TABLE_NAME + 
                " (" +
                USER_ID + " INTEGER PRIMARY KEY, " +
                USER_USERNAME + " TEXT UNIQUE, " +
                USER_PASSWORD + " TEXT NOT NULL, " +
                USER_FIRSTNAME + " TEXT, " +
                USER_LASTNAME + " TEXT, " +
                USER_COUNTRY + " TEXT, " +
                USER_CITY + " TEXT " +
                ");" ;
    private static final String USER_TABLE_DELETE = 
    		"DROP TABLE IF EXISTS " + USER_TABLE_NAME;
    
    public static final String CATEGORY_TABLE_NAME = "category";
    public static final String CATEGORY_ID = "id";
    public static final String CATEGORY_NAME = "name";
    private static final String CATEGORY_TABLE_CREATE =
            "CREATE TABLE " + CATEGORY_TABLE_NAME + 
            " (" +
            	CATEGORY_ID + " INTEGER PRIMARY KEY, " +
            	CATEGORY_NAME + " TEXT NOT NULL " +
            ");" ;
    private static final String CATEGORY_TABLE_DELETE = 
    		"DROP TABLE IF EXISTS " + CATEGORY_TABLE_NAME;
    
    public static final String DEVICE_TABLE_NAME = "device";
    public static final String DEVICE_ID = "id";
    public static final String DEVICE_BOARD = "board";
    public static final String DEVICE_BRAND = "brand";
    public static final String DEVICE_DEVICE = "device";
    public static final String DEVICE_CPU_ABI = "cpu_abi";
    public static final String DEVICE_CPU_ABI2 = "cpu_abi2";
    public static final String DEVICE_MANUFACTURER = "manufacturer";
    public static final String DEVICE_MODEL = "model";
    public static final String DEVICE_PRODUCT = "product";
    public static final String DEVICE_HARDWARE = "hardware";
    public static final String DEVICE_BOOTLOADER = "bootloader";
    public static final String DEVICE_SDK = "sdk";
    private static final String DEVICE_TABLE_CREATE =
                "CREATE TABLE " + DEVICE_TABLE_NAME + 
                " (" +
	                DEVICE_ID + " INTEGER PRIMARY KEY, " +
	                DEVICE_BOARD + " TEXT, " +
	                DEVICE_BRAND + " TEXT, " +
	                DEVICE_DEVICE + " TEXT, " +
	                DEVICE_CPU_ABI + " TEXT, " +
	                DEVICE_MANUFACTURER + " TEXT, " +
	                DEVICE_MODEL + " TEXT, " +
	                DEVICE_PRODUCT + " TEXT, " +
	                DEVICE_SDK + " INTEGER, " +
	                DEVICE_BOOTLOADER + " TEXT, " +
	                DEVICE_CPU_ABI2 + " TEXT, " +
	                DEVICE_HARDWARE + " TEXT " +
                ");" ;
    private static final String DEVICE_TABLE_DELETE = 
    		"DROP TABLE IF EXISTS " + DEVICE_TABLE_NAME;
    
    public static final String TEST_TABLE_NAME = "test";
    public static final String TEST_ID = "id";
    public static final String TEST_NAME = "name";
    public static final String TEST_DATE = "date";
    public static final String TEST_DURATION = "duration";
    public static final String TEST_SCORE = "score";
    public static final String TEST_CATEGORY_ID = "category_id";
    private static final String TEST_TABLE_CREATE =
            "CREATE TABLE " + TEST_TABLE_NAME + 
            " (" +
                TEST_ID + " INTEGER PRIMARY KEY, " +
                TEST_NAME + " TEXT NOT NULL, " +
                TEST_DATE + " TEXT NOT NULL, " +
                TEST_DURATION + " INTEGER NOT NULL, " +
                TEST_SCORE + " INTEGER NOT NULL, " +
                TEST_CATEGORY_ID + " INTEGER, " +
                "FOREIGN KEY("+ TEST_CATEGORY_ID +") REFERENCES " + CATEGORY_TABLE_NAME + "("+ CATEGORY_ID +")" +
            ");" ;
    private static final String TEST_TABLE_DELETE = 
    		"DROP TABLE IF EXISTS " + TEST_TABLE_NAME;
    
    public static final String USER_TEST_TABLE_NAME = "user_test";
    public static final String USER_TEST_USER_ID = "user_id";
    public static final String USER_TEST_TEST_ID = "test_id";
    private static final String USER_TEST_TABLE_CREATE =
            "CREATE TABLE " + USER_TEST_TABLE_NAME + 
            " (" +
            	USER_TEST_USER_ID + " INTEGER, " +
            	USER_TEST_TEST_ID + " INTEGER, " +
	            "FOREIGN KEY("+ USER_TEST_USER_ID +") REFERENCES " + USER_TABLE_NAME + "("+ USER_ID +"), " +
	            "FOREIGN KEY("+ USER_TEST_TEST_ID +") REFERENCES " + TEST_TABLE_NAME + "("+ TEST_ID +")" +
            ");" ;
    private static final String USER_TEST_TABLE_DELETE = 
    		"DROP TABLE IF EXISTS " + USER_TEST_TABLE_NAME;
    
    public static final String USER_DEVICE_TABLE_NAME = "user_device";
    public static final String USER_DEVICE_USER_ID = "user_id";
    public static final String USER_DEVICE_DEVICE_ID = "device_id";
    private static final String USER_DEVICE_TABLE_CREATE =
            "CREATE TABLE " + USER_DEVICE_TABLE_NAME + 
            " (" +
            	USER_DEVICE_USER_ID + " INTEGER, " +
            	USER_DEVICE_DEVICE_ID + " INTEGER, " +
                "FOREIGN KEY("+ USER_DEVICE_USER_ID +") REFERENCES " + USER_TABLE_NAME + "("+ USER_ID +"), " +
                "FOREIGN KEY("+ USER_DEVICE_DEVICE_ID +") REFERENCES " + DEVICE_TABLE_NAME + "("+ DEVICE_ID +")" +
            ");" ;
    private static final String USER_DEVICE_TABLE_DELETE = 
    		"DROP TABLE IF EXISTS " + USER_DEVICE_TABLE_NAME;
    
	public DtbOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(USER_TABLE_CREATE);
		db.execSQL(CATEGORY_TABLE_CREATE);
		db.execSQL(DEVICE_TABLE_CREATE);
		db.execSQL(TEST_TABLE_CREATE);
		db.execSQL(USER_TEST_TABLE_CREATE);
		db.execSQL(USER_DEVICE_TABLE_CREATE);
		
		populateCategories(db);
		populateDevice(db);
		populateUser(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(USER_DEVICE_TABLE_DELETE);
		db.execSQL(USER_TEST_TABLE_DELETE);
		db.execSQL(TEST_TABLE_DELETE);
		db.execSQL(DEVICE_TABLE_DELETE);
		db.execSQL(CATEGORY_TABLE_DELETE);
		db.execSQL(USER_TABLE_DELETE);
		onCreate(db);
	}
	
	private void populateCategories(SQLiteDatabase db){
		ContentValues val = new ContentValues();
		
		val.put(CATEGORY_NAME, "cpu");
		db.insert(CATEGORY_TABLE_NAME, null, val);
		val.clear();
		
		val.put(CATEGORY_NAME, "ram");
		db.insert(CATEGORY_TABLE_NAME, null, val);
		val.clear();
		
		val.put(CATEGORY_NAME, "gpu");
		db.insert(CATEGORY_TABLE_NAME, null, val);
		val.clear();
		
		val.put(CATEGORY_NAME, "memory");
		db.insert(CATEGORY_TABLE_NAME, null, val);
		val.clear();
	}
	private void populateUser(SQLiteDatabase db){
		ContentValues val = new ContentValues();
		val.put(USER_USERNAME, "anonymous");
		val.put(USER_PASSWORD, "anonymous");
		db.insert(USER_TABLE_NAME, null, val);
	}
	@SuppressLint("NewApi")
	private void populateDevice(SQLiteDatabase db){
		ContentValues val = new ContentValues();
		val.put(DEVICE_BOARD, android.os.Build.BOARD);
		val.put(DEVICE_BRAND, android.os.Build.BRAND);
		val.put(DEVICE_CPU_ABI, android.os.Build.CPU_ABI);
		val.put(DEVICE_DEVICE, android.os.Build.DEVICE);
		val.put(DEVICE_MANUFACTURER, android.os.Build.MANUFACTURER);
		val.put(DEVICE_MODEL, android.os.Build.MODEL);
		val.put(DEVICE_PRODUCT, android.os.Build.PRODUCT);
		val.put(DEVICE_SDK, android.os.Build.VERSION.SDK_INT);
		if (android.os.Build.VERSION.SDK_INT >= 8) {
			val.put(DEVICE_BOOTLOADER, android.os.Build.BOOTLOADER);
			val.put(DEVICE_CPU_ABI2, android.os.Build.CPU_ABI2);
			val.put(DEVICE_HARDWARE, android.os.Build.HARDWARE);
		}
		db.insert(DEVICE_TABLE_NAME, null, val);
	}

}
