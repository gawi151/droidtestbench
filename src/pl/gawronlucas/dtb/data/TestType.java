package pl.gawronlucas.dtb.data;

/**
 * 
 * @author �ukasz Gawron
 *
 */

public final class TestType {
	public static final String CPU = "cpu";
	public static final String RAM = "ram";
	public static final String OPEN_GL = "opengl";
	public static final String DATABASE = "database";
	public static final String SD_CARD = "sd card";
}
