package pl.gawronlucas.dtb.data;

import java.util.ArrayList;

import android.os.Build;

public class Device {

	public static final String BRAND = Build.BRAND;
	public static final String BOARD = Build.BOARD;
	public static final String CPU_ABI = Build.CPU_ABI;
	public static final String DEVICE = Build.DEVICE;
	public static final String DISPLAY = Build.DISPLAY;
	public static final String MANUFACTURER = Build.MANUFACTURER;
	public static final String MODEL = Build.MODEL;
	public static final String PRODUCT = Build.PRODUCT;

	public ArrayList<String> deviceData = new ArrayList<String>();

	public Device() {
		prepareData();
	}

	private void prepareData() {
		deviceData.add("Board: " + BOARD);
		deviceData.add("Brand: " + BRAND);
		deviceData.add("Cpu: " + CPU_ABI);
		deviceData.add("Device: " + DEVICE);
		deviceData.add("Display: " + DISPLAY);
		deviceData.add("Manufacturer: " + MANUFACTURER);
		deviceData.add("Model: " + MODEL);
		deviceData.add("Product: " + PRODUCT);
	}

}
