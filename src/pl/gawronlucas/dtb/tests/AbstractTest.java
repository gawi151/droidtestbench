package pl.gawronlucas.dtb.tests;

import pl.gawronlucas.dtb.tests.math.BigMath;

/**
 * 
 * @author �ukasz Gawron
 *
 */

public abstract class AbstractTest {

	private BigMath math;
	
	public AbstractTest() {
		setMath(new BigMath());
	}

	/**
	 * @return the math
	 */
	protected BigMath getMath() {
		return math;
	}

	/**
	 * @param math the math to set
	 */
	protected void setMath(BigMath math) {
		this.math = math;
	}

}
