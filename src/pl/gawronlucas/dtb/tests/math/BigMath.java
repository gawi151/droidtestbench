package pl.gawronlucas.dtb.tests.math;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * public class <br />
 * <h1>BigMath</h1>
 * <p>Klasa zawierajaca funkcje do oblicze� na du�ych typach liczb: {@link BigDecimal} i {@link BigInteger}.
 * Wszystkie funkcje matematyczne napisane sa od nowa specjalnie dla duzych typow liczb
 * po to aby uzyskac jak nawieksza dokladnosc obliczen, potrzebnych do wykonywania testow.
 * </p>
 *
 * @author �ukasz Gawron
 * @version 0.0.1, 10.12.2013
 * @see {@link BigDecimal}, {@link BigInteger}
 */
// TODO: dokonczyc pisac metody liczace
/*
 * pierwiastkowanie
 * silnia
 * itp
 */
public class BigMath {

	/**
	 * Default constructor
	 */
	public BigMath(){
		
	}
	/**
	 * Oblicza potege dla rzeczywistej podstawy i calkowitego wykladnika.
	 * @param base - podstawa potegowania, ca�kowita
	 * @param power - wykladnik potegowania, naturalny
	 * @return zwraca wynik potegowania base^power
	 * @see {@link BigDecimal}, {@link BigInteger}
	 */
	public static BigDecimal pow(BigDecimal base, BigInteger power) {
		BigDecimal result = BigDecimal.ONE;
		while (power.compareTo(BigInteger.ZERO) > 0) {
			BigInteger i = power.and(BigInteger.ONE);
			if (!i.equals(BigInteger.ZERO))
				result = result.multiply(base);
			base = base.multiply(base);
			power = power.shiftRight(1);
		}
		return result;
	}
	
}
