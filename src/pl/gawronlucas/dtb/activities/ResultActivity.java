package pl.gawronlucas.dtb.activities;

/**
 * 
 * @author �ukasz Gawron
 *
 */

import pl.gawronlucas.dtb.R;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;

// TODO: ResultActivity do skonczenia
public class ResultActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.result, menu);
		return true;
	}

}
