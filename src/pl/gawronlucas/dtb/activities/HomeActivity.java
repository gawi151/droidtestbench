package pl.gawronlucas.dtb.activities;

/**
 * 
 * @author Łukasz Gawron
 *
 */

import pl.gawronlucas.dtb.R;
import pl.gawronlucas.dtb.db.DtbOpenHelper;
import pl.gawronlucas.dtb.fragments.*;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;

// TODO: HomeActivity do skonczenia
public class HomeActivity extends ActionBarActivity implements
		HomeFragment.OnFragmentInteractionListener,
		TestFragment.OnFragmentInteractionListener,
		ResultsListFragment.OnFragmentInteractionListener,
		DeviceFragment.OnFragmentInteractionListener,
		DeviceThumbnailFragment.OnFragmentInteractionListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		/*
		 * zainizjowanie bazy danych po raz pierwszy jesli nie zostala
		 * utworzona, w innym przypadku nic nie robi
		 */
		new Thread(new Runnable() {
			public void run() {
				DtbOpenHelper dboh = new DtbOpenHelper(getBaseContext());
				dboh.getReadableDatabase();
				dboh.close();
			}
		});

		// utworzenie action bara + dodanie tab'ow
		ActionBar ab = getSupportActionBar();
		ab.setDisplayShowTitleEnabled(false); // ukrycie tytulu HomeActivity
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		ab.addTab(ab
				.newTab()
				.setText("Home")
				.setTabListener(
						new TabListener<HomeFragment>(this, "home",
								HomeFragment.class)));
		ab.addTab(ab
				.newTab()
				.setText("Test")
				.setTabListener(
						new TabListener<TestFragment>(this, "tests",
								TestFragment.class)));
		ab.addTab(ab
				.newTab()
				.setText("Results")
				.setTabListener(
						new TabListener<ResultsListFragment>(this, "tests",
								ResultsListFragment.class)));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater mInflater = getMenuInflater();
		mInflater.inflate(R.menu.home, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * TabListner odpowiada za nasĹuchiwanie i przeĹÄczanie fragmentĂłw
	 * odpowiednio to przyporzÄdkowanych tabĂłw.
	 * 
	 * @param <T>
	 *            Fragment uĹźyty do nasĹuchiwania zmian w action bar'ze
	 * @author Lucas
	 */
	public static class TabListener<T extends Fragment> implements
			ActionBar.TabListener {
		private Fragment mFragment;
		private final Activity mActivity;
		private final String mTag;
		private final Class<T> mClass;

		/**
		 * Constructor used each time a new tab is created.
		 * 
		 * @param activity
		 *            The host Activity, used to instantiate the fragment
		 * @param tag
		 *            The identifier tag for the fragment
		 * @param clz
		 *            The fragment's Class, used to instantiate the fragment
		 */
		public TabListener(Activity activity, String tag, Class<T> clz) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
		}

		/* The following are each of the ActionBar.TabListener callbacks */
		public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
			// Check if the fragment is already initialized
			if (mFragment == null) {
				// If not, instantiate and add it to the activity
				mFragment = Fragment.instantiate(mActivity, mClass.getName());
				//ft.replace(android.R.id.content, mFragment, mTag);
				ft.add(android.R.id.content, mFragment, mTag);
			} else {
				// If it exists, simply attach it in order to show it
				//ft.replace(android.R.id.content, mFragment, mTag);
				ft.attach(mFragment);
			}
		}

		public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
			if (mFragment != null) {
				//ft.remove(mFragment);
				ft.detach(mFragment);
			}
		}

		public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
			// User selected the already selected tab. Usually do nothing.
		}
	}

	@Override
	public void onFragmentInteraction(Uri uri) {

	}

}
